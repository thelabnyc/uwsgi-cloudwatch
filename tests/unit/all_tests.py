from unittest import TestCase
from uwsgi_cloudwatch.main import generate_metrics, retrieve_stats
from tests import load_fixture


class AllTests(TestCase):

    def test_generate_metrics(self):
        stats = load_fixture('stats')
        m = generate_metrics(stats)

        self.assertEqual(m['ListenQueueCount'], 0)
        self.assertEqual(m['LoadCount'], 0)

        self.assertEqual(m['WorkersCount'], 2)
        self.assertEqual(m['WorkersBusyCount'], 2)
        self.assertEqual(m['WorkersRunningCount'], 2)

        self.assertEqual(len(m['WorkersAverageResponseTimeMilliseconds']), 2)
        self.assertEqual(m['WorkersAverageResponseTimeMilliseconds'][0], 3017)
        self.assertEqual(m['WorkersAverageResponseTimeMilliseconds'][1], 3017)
