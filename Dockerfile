FROM python:3.8

# Create directories
RUN mkdir -p /code
WORKDIR /code

# Install requirements
VOLUME /root/.cache/pip
ADD requirements.txt /code/
RUN pip install -r requirements.txt

# Add source
ADD . /code/

# Install package
RUN pip install -e .

# Run as a non-root user
USER www-data:root

# Set Docker command
CMD ["uwsgi-cloudwatch"]
