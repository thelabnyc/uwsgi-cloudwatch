import arrow
import asyncio
import boto3
import click
import logging
import re
import requests
import signal
from . import validation

logging.basicConfig(level=logging.INFO)


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def run_periodically(func, interval):
    """ Runs a task periodically. """
    _loop = asyncio.get_event_loop()

    def _set():
        _loop.call_later(interval, _run)

    def _run():
        func()
        _set()

    _set()


def put_metrics(metrics, region, namespace, metric_prefix, dimensions):
    """
    Puts metrics in target CloudWatch namespace.
    """
    timestamp = arrow.get().datetime
    client = boto3.client('cloudwatch', region_name=region)

    # Assemble Metrics
    metric_data = []
    for name, value in metrics.items():

        # Detect Units
        if re.match(".*Bytes", name):
            unit = "Bytes"
        elif re.match(".*Count", name):
            unit = "Count"
        elif re.match(".*Milliseconds", name):
            unit = "Milliseconds"
        else:
            unit = "None"

        if type(value) == list:
            for i in value:
                metric_data.append({
                    'MetricName': metric_prefix + name,
                    'Dimensions': dimensions,
                    'Timestamp': timestamp,
                    'Value': i,
                    'Unit': unit
                })
        else:
            metric_data.append({
                'MetricName': metric_prefix + name,
                'Dimensions': dimensions,
                'Timestamp': timestamp,
                'Value': value,
                'Unit': unit
            })

    # Put Metrics
    for chunk in chunks(metric_data, 20):
        client.put_metric_data(
            Namespace=namespace,
            MetricData=chunk
        )


def generate_metrics(stats):
    """
    Generates metrics from uWSGI stats.
    """
    m = {}
    m['ListenQueueCount'] = stats['listen_queue']
    m['LoadCount'] = stats['load']

    # Worker Metrics
    m['WorkersCount'] = len(stats['workers'])
    m['WorkersBusyCount'] = 0
    m['WorkersRunningCount'] = 0
    m['WorkersAverageResponseTimeMilliseconds'] = []
    for worker in stats['workers']:
        if worker['status'] == 'busy':
            m['WorkersBusyCount'] += 1
        if worker['status'] in ['idle', 'busy']:
            m['WorkersRunningCount'] += 1
        m['WorkersAverageResponseTimeMilliseconds'].append(int(worker['avg_rt']/1000))

    return m


def retrieve_stats(stats_server):
    """
    Retrieve latest stats from uWSGI Stats Server.
    """
    logging.info('Retrieving stats...')
    response = requests.get(stats_server)
    return response.json()


def update_cloudwatch_metrics(stats_server, region, namespace, metric_prefix, dimensions):
    """
    Update a CloudWatch namespace with the latest metrics generated from uWSGI Stats Server.
    """
    def f():
        try:
            stats = retrieve_stats(stats_server)
        except Exception as e:
            logging.error("Failed to retrieve uWSGI stats: %s" % e)
        try:
            metrics = generate_metrics(stats)
            put_metrics(metrics, region, namespace, metric_prefix, dimensions)
        except Exception as e:
            logging.error("Failed to put metrics: %s" % e)

    return f


@click.command()
@click.argument('stats-server')
@click.option('--region', default='us-east-1')
@click.option('--namespace', required=True, callback=validation.namespace)
@click.option('--frequency', default=60, callback=validation.frequency)
@click.option('--metric-prefix', default='uWSGI', callback=validation.prefix)
@click.option('--dimension', multiple=True, nargs=2, type=click.Tuple([str, str]))
def cli(stats_server, region, namespace, frequency, metric_prefix, dimension):
    # Use asyncio event loop to periodically poll uWSGI stats
    metric_dimensions = [dict(Name=key, Value=value) for key, value in dict(dimension).items()]
    update_func = update_cloudwatch_metrics(stats_server, region, namespace, metric_prefix, metric_dimensions)
    run_periodically(update_func, frequency)
    # Setup an event handler to gracefully stop when we get a SIGTERM
    loop = asyncio.get_event_loop()
    loop.add_signal_handler(signal.SIGTERM, lambda: loop.stop())
    # Run the event loop
    loop.run_forever()
